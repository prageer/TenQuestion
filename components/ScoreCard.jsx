import React, { Component } from 'react';
import PropTypes from 'prop-types'; 

import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import {List, ListItem} from 'material-ui/List';
import DatePicker from 'material-ui/DatePicker';
import TextField from 'material-ui/TextField';

import ScoreCardItem from './ScoreCardItem';
import update from 'react-addons-update';

import {answerItem} from '../reducers/ref/componentAnswerUnitState';

const styles = {
  titleStyle: {
    fontSize:'25px', 
    lineHeight:'50px', 
    color:'white'
  },
  totalLabelStyle: {
    fontSize:"20px", 
    textAlign:"right", 
    marginRight:"10px"
  },
  totalStyle:{
    cursor:"normal", 
    height:"40px", 
    textAlign:"right", 
    fontSize:"22px", 
    width:"100px"
  },
  totalInputStyle:{
    textAlign: 'right', 
    color:"black"
  }
};

const titleAnswersState = {
    itemGroups: [
        {
            title: 'FEEDING',
            selectedValue:"",
            selectedWeight:null,
            items: [
                { weight: 0, name: 'Unable' },
                { weight: 5, name: 'Needs help cutting, spreading butter, etc., or requires modified diet' },
                { weight: 10, name: 'Independent' }
            ]
        },
        {
            title: 'BATHING',
            selectedValue:"",
            selectedWeight:null,
            items: [
                { weight: 0, name: 'Dependent' },
                { weight: 5, name: 'Independent (or in shower)' }
            ]
        },
        {
            title: 'GROOMING',
            selectedValue:"",
            selectedWeight:null,
            items: [
                { weight: 0, name: 'Needs to help with personal care' },
                { weight: 5, name: 'Independent face/hair/teeth/shaving (implements provided)'}
            ]
        },
        {
            title: 'DRESSING',
            selectedValue:"",
            selectedWeight:null,
            items: [
                { weight: 0, name: 'Dependent' },
                { weight: 5, name: 'Needs help but can do about half unaided' },
                { weight: 10, name: 'Independent (including buttons, zips, laces, etc.)' }
            ]
        },
        {
            title: 'BOWELS',
            selectedValue:"",
            selectedWeight:null,
            items: [
                { weight: 0, name: 'Incontinent (or needs to be given enemas)' },
                { weight: 5, name: 'Occasional accident' },
                { weight: 10, name: 'Continent' }
            ]
        },
        {
            title: 'BLADDER',
            selectedValue:"",
            selectedWeight:null,
            items: [
                { weight: 0, name: 'Incontinent, or catheterized and unable to manage alone' },
                { weight: 5, name: 'Occasional accident' },
                { weight: 10, name: 'Continent' }
            ]
        },
        {
            title: 'TOILET USE',
            selectedValue:"",
            selectedWeight:null,
            items: [
                { weight: 0, name: 'Dependent' },
                { weight: 5, name: 'Needs some help, but can do something alone' },
                { weight: 10, name: 'Independent (on and off, dressing, wiping)' }
            ]
        },
        {
            title: 'TRANSFERS (BED TO CHAIR AND BACK)',
            selectedValue:"",
            selectedWeight:null,
            items: [
                { weight: 0, name: 'Unable, no sitting balance' },
                { weight: 5, name: 'Major help (one or two people, physical), can sit' },
                { weight: 10, name: 'Minor help (verbal or physical)' },
                { weight: 15, name: 'Independent' }
            ]
        },
        {
            title: 'MOBILITY (ON LEVEL SURFACES)',
            selectedValue:"",
            selectedWeight:null,
            items: [
                { weight: 0, name: 'Immobile or < 50 yards' },
                { weight: 5, name: 'Wheelchair independent, including corners, > 50 yards' },
                { weight: 10, name: 'Walks with help of one person (verbal or physical) > 50 yards' },
                { weight: 15, name: 'Independent (but may use any aid; for example, stick) > 50 yards' }
            ]
        },
        {
            title: 'STAIRS',
            selectedValue:"",
            selectedWeight:null,
            items: [
                { weight: 0, name: 'Unable' },
                { weight: 5, name: 'Needs help (verbal, physical, carrying aid)' },
                { weight: 10, name: 'Independent' }
            ]
        }
    ]
  }

class ScoreCard extends Component {  

  constructor(props){
    super(props);   

    this.state={
      itemGroups:titleAnswersState.itemGroups
    }     
  }

  componentWillMount() {
    this.props.actions.setPatientID(this.props.patientPropsID);   
    this.props.actions.setPractitionerID(this.props.practitionerPropsID);      
  }

  selectItemWeight( selectedValue, itemWeight, groupIndex ){

    if (typeof groupIndex !== 'undefined' && typeof itemWeight !== 'undefined' && typeof selectedValue !== 'undefined') {

        let newState =  update(this.state, {
            itemGroups:{
              [groupIndex]:{
                selectedWeight: { $set: itemWeight }
              }
            }
        });

        let weightArr = newState.itemGroups.map((item,index)=>item.selectedWeight )
        let totalScore = weightArr.reduce((a, b) => a + b, 0);
        let title = this.state.itemGroups[groupIndex].title;

        let exists = (this.state.itemGroups[groupIndex].selectedValue ==="")?false:true;

        newState = update(this.state, {
            total:{$set:totalScore},
            itemGroups:{
              [groupIndex]:{
                selectedValue: { $set: selectedValue },
                selectedWeight: { $set: itemWeight }
              }
            }
        });
        

        // get answerUnitState
        let answerItemState = answerItem(title, itemWeight, selectedValue);

        // get wheter this question is final
        let currentSelectedCount =  this.state.itemGroups.filter(group=>group.selectedValue!=="" ).length;
        let selectedCount = (!exists)?currentSelectedCount+1:currentSelectedCount;
        let final = (selectedCount===this.state.itemGroups.length)?"final":"";

        this.setState(newState, ()=>{ this.props.actions.setAnswer(exists, answerItemState, totalScore, final); });

    } 
  }

  render() {

    const { patientID, date, patientPropsID, practitionerID, totalScore, final } = this.props;    
    const { changeDate } = this.props;

    if( !patientID || !practitionerID)
      return null;

    let dt = (date)?new Date(date*1000):new Date();
    let totalScoreStr = (totalScore===null)?"":totalScore.toString();
    
    let totalInput = (
        <table width="100%">
          <tbody>
            <tr>
              <td width="60%">
                <p style={styles.totalLabelStyle}>Total Scores:</p>
              </td>
              <td>
                <TextField          
                  name="totalInput"
                  value={totalScoreStr}
                  disabled={true}
                  errorText={ final ==="" ? "Check all items" : "" }
                  style={styles.totalStyle}
                  inputStyle={styles.totalInputStyle}
                />
              </td>
            </tr>
          </tbody>
        </table>
      );    

    return (
      <div className="parentBox" >
          
          <Card>
            <CardHeader
              title="Barthel Index Scoring"                            
              className="cntHeader"
              titleStyle={styles.titleStyle}
            />           
            <CardActions>              
              <DatePicker
                floatingLabelText="Date"
                defaultDate={dt}
                onChange = {changeDate}
              />
            </CardActions>          
            
            <List>
              {this.state.itemGroups.map((item, index)=>
                <ScoreCardItem 
                  key={index} 
                  group={item} 
                  groupIndex={index} 
                  selectItemWeight={this.selectItemWeight.bind(this)} 
                />
              )}            
            </List>
            
            {totalInput}

          </Card>
      </div>
    );
  }
}

ScoreCard.propTypes = {
  patientID:PropTypes.string, 
  patientPropsID:PropTypes.string.isRequired,  //from props
  date:PropTypes.number.isRequired,  
  practitionerID:PropTypes.string,
  practitionerPropsID:PropTypes.string.isRequired,  //from props
  final:PropTypes.string, 
  changeDate:PropTypes.func.isRequired
};

export default ScoreCard;
