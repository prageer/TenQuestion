import React from 'react';
import PropTypes from 'prop-types'; 
import Subheader from 'material-ui/Subheader';
import {List, ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import Divider from 'material-ui/Divider';


const styles = {
  radioButton: {
    marginBottom: 16
  },
  itemBack:{  	
  	fontSize:"18px"
  }
};

import {
  cyan200
} from 'material-ui/styles/colors';

class ScoreCardItem extends React.Component{
	
	changeValue(obj,value){		
		let selectedItem = this.props.group.items.filter(piece=>piece.name===value );
		this.props.selectItemWeight( selectedItem[0].name, selectedItem[0].weight, this.props.groupIndex );
	}

	render(){

		//console.log(this.props.group);

		const {title, items, selectedValue} = this.props.group;		
		const groupIndex = this.props.groupIndex+1;	

		let radioItem = 
			(
				<RadioButtonGroup 
					name={title} 
					valueSelected={selectedValue} 
					onChange={this.changeValue.bind(this)}
				>
					{items.map((item, index)=>
						<RadioButton
							key={index}
							value={item.name}
						  	label={item.name}
						  	style={styles.radioButton}
						/>
					)}
				</RadioButtonGroup>
			)
		

		let listItem = (
				<ListItem
	              disabled={true}	              
	            >
	            	{radioItem}
	            </ListItem>
	           );
	    
	    let	listItemWithSelected = (
	    		<ListItem
	              disabled={true}
	              leftAvatar={<Avatar size={40} backgroundColor={cyan200}>{groupIndex}</Avatar>}
	            >	
	            	{radioItem}
	            </ListItem>
	          );


		return (
			<div>
				<Subheader style={styles.itemBack}>{title}</Subheader>

	            { (selectedValue !=="" )? listItemWithSelected: listItem }				
	            
	            <Divider />
            </div>
		)
	}
}

ScoreCardItem.propTypes = {
  group: PropTypes.object,
  selectItemWeight:PropTypes.func,
  groupIndex:PropTypes.number
};

ScoreCardItem.defaultProps={
	group:[]
}

export default ScoreCardItem;