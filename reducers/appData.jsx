import { SET_DATE, SET_PATIENT_ID, SET_ANSWER, SET_PRACTITIONER_ID } from '../constants/ActionTypes';
import update from 'react-addons-update';

const initialState = 
{
    resourceType: "Observation",        
    contained: [
        {   
            resourceType: "Patient",
            id: ""//this.props.patientID
        }
    ],
    status: "",   // final
    category: [
        {
          coding: [
            {
              system: "http://hl7.org/fhir/observation-category",
              code: "survey",
              display: "Survey"
            }
          ],
          text: "Survey"
        }
    ],
    code: {
    coding: [
            {
                system: "https://uts.nlm.nih.gov/",
                code: "C0451019",
                display: "Barthel index"
            },
            {
                system: "http://snomed.info/sct",
                code: "273302005",
                display: "Barthel Index"
            }
        ],
        text: "Barthel Index"
    },
    effectiveDateTime: Math.round(new Date().getTime()/1000),
    performer: [
        {
            reference: ""   // this.props.practitionerID
        }
    ],
    valueQuantity: {
        value:null,    // totla score,
        system: "http://unitsofmeasure.org",
        code: "{score}"
    },
    component: [       // array of answerUnitState
        
    ]
}

export default function appData(state = initialState, action) {
  if(typeof state === "undefined")
        state = initialState;

  switch (action.type) {
      case SET_DATE:      
        if (typeof action.date !== 'undefined') {
            return update(state, {
                effectiveDateTime: { $set: action.date }            
            });
        } else {
            return state;
        }

      case SET_PATIENT_ID:      

        if (typeof action.patientID !== 'undefined') {
            return update(state, {
                contained: { 
                    [0]:{
                        id:{ $set: action.patientID }
                    }
                }
            });
        } else {
            return state;
        }

      case SET_PRACTITIONER_ID:        
        if (typeof action.practitionerID !== 'undefined') {
            return update(state, {
                performer: { 
                    [0]:{
                        reference:{ $set: action.practitionerID }
                    }
                }
            });
        } else {
            return state;
        }

      case SET_ANSWER:      
        if (typeof action.exists !== 'undefined' && typeof action.answerItem !== 'undefined' && typeof action.totalScore !== 'undefined' && typeof action.final !== 'undefined') {
            //Update if exists or add new element

            if(action.exists){

                let answerIndex = null;
                state.component.map((item, index)=>{
                    if(item.code.text === action.answerItem.code.text)
                        answerIndex = index;
                });

                return update(state, {
                    valueQuantity:{
                        value:{$set: action.totalScore}
                    },
                    component: { 
                        [answerIndex]:{
                           $set: action.answerItem
                        }
                    },
                    status: { $set: action.final }
                });

            }else{

                let pos = state.component.length;
                let newComponent = update(state.component, {$splice: [[pos, 0, action.answerItem]]});

                return update(state, {
                    valueQuantity:{
                        value:{$set: action.totalScore}
                    },
                    component: {$set:newComponent},
                    status: { $set: action.final }
                });

            }
            
        } else {
            return state;
        }
        
      default:
        return state;
  }
}
