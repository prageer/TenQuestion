export const answerItem = (title, itemWeight, selectedValue)=>{    
    return {
        code: {
            coding: [
                {
                    system: "https://uts.nlm.nih.gov/",
                    code: "C1691017",
                    display: "Component of Barthel index scale"
                }
            ],
            text: title
        },
        valueCodeableConcept: {
            coding: [
                {
                    extension:[
                        {
                            url: "http://hl7.org/fhir/StructureDefinition/iso21090-CO-value",
                            valueDecimal:itemWeight
                        }
                    ],
                    system: "https://uts.nlm.nih.gov/",
                    code: "C1691017",
                    display: "Component of Barthel index scale"
                }
            ],
            text:selectedValue
        }
    }
    
}