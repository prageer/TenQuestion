import React, { Component } from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types'; 
import ScoreCard from '../components/ScoreCard';

import * as itemActions from '../actions/itemWeight';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import theme from '../src/material_ui_raw_theme_file'



class App extends Component {

  changePatientName(e){
    this.props.actions.setPatientName(e.target.value);    
  }

  changeDate(e, dateString){  
    let stamp = Date.parse(dateString);
    stamp = stamp/1000;
    this.props.actions.setDate(stamp);
  }

  render() {

    const { date, patientID, practitionerID, totalScore, final } = this.props;
    const { actions } = this.props;


    return (
      <div>
        <MuiThemeProvider muiTheme={theme}>
          <div>
            <ScoreCard 
              date={date}
              patientID={patientID}
              practitionerID={practitionerID}
              patientPropsID="3"
              practitionerPropsID="4"
              totalScore={totalScore}
              actions = {actions}
              final = {final}
              changeDate = {this.changeDate.bind(this)}
            />
          </div>
        </MuiThemeProvider>
      </div>
    );
  }
}

App.propTypes = {  
  date: PropTypes.number.isRequired,
  patientID: PropTypes.string,
  practitionerID: PropTypes.string,
  totalScore:PropTypes.number,
  final:PropTypes.string
};

function mapStateToProps(state) {
  return {    
    date:     state.appData.effectiveDateTime,
    patientID:state.appData.contained[0].id,
    practitionerID:state.appData.performer[0].reference,
    totalScore:state.appData.valueQuantity.value,
    final:state.appData.status
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(itemActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
