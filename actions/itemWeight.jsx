import * as types from '../constants/ActionTypes';

export function setDate(date) {
  return { type: types.SET_DATE, date };
}

export function setPatientID(patientID) {
  return { type: types.SET_PATIENT_ID, patientID };
}

export function setPractitionerID(practitionerID) {
  return { type: types.SET_PRACTITIONER_ID, practitionerID };
}

export function setAnswer(exists, answerItem, totalScore, final) {
  return { type: types.SET_ANSWER, exists, answerItem, totalScore, final };
}