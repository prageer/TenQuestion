import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';

import ScoreCardItem from '../components/ScoreCardItem';

describe('<ScoreCardItem/> Component', function () {
 	
	const props = {                                                                                                                                                   
	    group: {
	    	title:"Feeding", 
	    	selectedValue:"", 
	    	items:[
	    		{name:"Unable", weight:10},
	    		{name:"Independent", weight:50}
	    	]
	    },
	    groupIndex:0
	  };

	it('should have props for question and answer', function () {  	
    	const wrapper = shallow(<ScoreCardItem  {...props } />);
    	expect(wrapper.props().group).to.be.defined;
  	});
  
  	it('should have as many radio buttons as answer length', function () {  	
    	const wrapper = shallow(<ScoreCardItem  {...props } />);
    	//expect(wrapper.find('RadioButton')).to.have.length( 2 );
    	expect(wrapper.find('RadioButton')).to.have.length( props.group.items.length );
  	});
  	
});