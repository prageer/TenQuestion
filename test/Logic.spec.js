import appData from '../reducers/appData'
import * as types from '../constants/ActionTypes'
import {expect} from 'chai';
import {answerItem} from '../reducers/ref/componentAnswerUnitState';


describe('Question/Answer Logic', () => {
  
	it('Set Date', () => {
	   let state = {effectiveDateTime:1490000000}

       state = appData(state,{
       		type:types.SET_DATE, 
       		date:1493837703
       	});
       expect(state).to.eql({effectiveDateTime:1493837703})
	})

	
	let state = {
		valueQuantity:{
            value:""
        },
        component: [],
        status: ""
	};

	it('Set Answer 1(10)/TotalScore 10', () => {
		let answerItemData = answerItem("Feeding", 10, "Independent"); // title, itemWeight, selected Answer
     
    	state = appData(state,{
	       		type:types.SET_ANSWER, 
	       		exists:false,	// update
	       		answerItem:answerItemData,	// answerData 
	       		totalScore:10,	// item weight
	       		final:""	// is final?
       		}
       	);       	
       	expect(state.valueQuantity.value).to.eql(10)       
	});

	it('Set Answer 2(5)/TotalScore 15', () => {
		let answerItemData = answerItem("BATHING", 5, "Independent (or in shower)"); // title, itemWeight, selected Answer
     
    	state = appData(state,{
	       		type:types.SET_ANSWER, 
	       		exists:false,	// update
	       		answerItem:answerItemData,	// answerData 
	       		totalScore:15,	// item weight
	       		final:""	// is final?
       		}
       	);
       	expect(state.valueQuantity.value).to.eql(15)       
	});

	it('Set Answer 3(0)/TotalScore 15', () => {
		let answerItemData = answerItem("GROOMING", 0, "Needs to help with personal care"); // title, itemWeight, selected Answer
     
    	state = appData(state,{
	       		type:types.SET_ANSWER, 
	       		exists:false,	// update
	       		answerItem:answerItemData,	// answerData 
	       		totalScore:15,	// item weight
	       		final:""	// is final?
       		}
       	);
       	expect(state.valueQuantity.value).to.eql(15)       
	});

	it('Set Answer 4(5)/TotalScore 20', () => {
		let answerItemData = answerItem("DRESSING", 5, "Needs help but can do about half unaided"); // title, itemWeight, selected Answer
     
    	state = appData(state,{
	       		type:types.SET_ANSWER, 
	       		exists:false,	// update
	       		answerItem:answerItemData,	// answerData 
	       		totalScore:20,	// item weight
	       		final:""	// is final?
       		}
       	);
       	expect(state.valueQuantity.value).to.eql(20)       
	});

	it('Set Answer 5(10)/TotalScore 30', () => {
		let answerItemData = answerItem("BOWELS", 10, "Continent"); // title, itemWeight, selected Answer
     
    	state = appData(state,{
	       		type:types.SET_ANSWER, 
	       		exists:false,	// update
	       		answerItem:answerItemData,	// answerData 
	       		totalScore:30,	// item weight
	       		final:""	// is final?
       		}
       	);
       	expect(state.valueQuantity.value).to.eql(30)       
	});

	it('Set Answer 6(5)/TotalScore 35', () => {
		let answerItemData = answerItem("BLADDER", 5, "Occasional accident"); // title, itemWeight, selected Answer
     
    	state = appData(state,{
	       		type:types.SET_ANSWER, 
	       		exists:false,	// update
	       		answerItem:answerItemData,	// answerData 
	       		totalScore:35,	// item weight
	       		final:""	// is final?
       		}
       	);
       	expect(state.valueQuantity.value).to.eql(35)       
	});

	it('Set Answer 7(0)/TotalScore 35', () => {
		let answerItemData = answerItem("TOILET USE", 0, "Dependent"); // title, itemWeight, selected Answer
     
    	state = appData(state,{
	       		type:types.SET_ANSWER, 
	       		exists:false,	// update
	       		answerItem:answerItemData,	// answerData 
	       		totalScore:35,	// item weight
	       		final:""	// is final?
       		}
       	);
       	expect(state.valueQuantity.value).to.eql(35)       
	});

	it('Set Answer 8(15)/TotalScore 50', () => {
		let answerItemData = answerItem("TRANSFERS (BED TO CHAIR AND BACK)", 15, "Independent"); // title, itemWeight, selected Answer
     
    	state = appData(state,{
	       		type:types.SET_ANSWER, 
	       		exists:false,	// update
	       		answerItem:answerItemData,	// answerData 
	       		totalScore:50,	// item weight
	       		final:""	// is final?
       		}
       	);
       	expect(state.valueQuantity.value).to.eql(50)       
	});

	it('Set Answer 9(0)/TotalScore 50', () => {
		let answerItemData = answerItem("MOBILITY (ON LEVEL SURFACES)", 0, "Immobile or < 50 yards"); // title, itemWeight, selected Answer
     
    	state = appData(state,{
	       		type:types.SET_ANSWER, 
	       		exists:false,	// update
	       		answerItem:answerItemData,	// answerData 
	       		totalScore:50,	// item weight
	       		final:""	// is final?
       		}
       	);
       	expect(state.valueQuantity.value).to.eql(50)       
	});

	it('Update Answer 1(5)/TotalScore 45', () => {
		let answerItemData = answerItem("FEEDING", 5, "Needs help cutting, spreading butter, etc., or requires modified diet"); // title, itemWeight, selected Answer
     
    	state = appData(state,{
	       		type:types.SET_ANSWER, 
	       		exists:true,	// update
	       		answerItem:answerItemData,	// answerData 
	       		totalScore:45,	// item weight
	       		final:""	// is final?
       		}
       	);
       	expect(state.valueQuantity.value).to.eql(45)       
	});

	it('Set Final Answer 10(5)/TotalScore 50', () => {
		let answerItemData = answerItem("STAIRS", 5, "Independent"); // title, itemWeight, selected Answer
     
    	state = appData(state,{
	       		type:types.SET_ANSWER, 
	       		exists:false,	// update
	       		answerItem:answerItemData,	// answerData 
	       		totalScore:50,	// item weight
	       		final:"final"	// is final?
       		}
       	);
       	expect(state.valueQuantity.value).to.eql(50)       
	});


})
