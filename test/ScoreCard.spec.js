import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';

import ScoreCard from '../components/ScoreCard';
import ScoreCardItem from '../components/ScoreCardItem';

describe('<ScoreCard/> Component', function () {

	const props = {                                                                                                                                                   
	    date:102222222,
		patientPropsID:"3",
		practitionerPropsID:"4",
		totalScore:100,
		final :"final",
		patientID:"3",
		practitionerID:"4",
		actions:{
			setPatientID:()=>{ props.patientID="3" },
			setPractitionerID:()=>{ props.practitionerID="4" }
		},
		changeDate:()=>{}
	  };

	it('should have a card header', function () {  	
    	const wrapper = shallow(<ScoreCard {...props} />);  
    	expect(wrapper.find('CardHeader')).to.have.length( 1 );
  	});

  	it('should have a datepicker', function () {  	
    	const wrapper = shallow(<ScoreCard {...props} />);  
    	expect(wrapper.find('DatePicker')).to.have.length( 1 );
  	});

  	it('should have a List', function () {  	
    	const wrapper = shallow(<ScoreCard {...props} />);  
    	expect(wrapper.find('List')).to.have.length( 1 );
  	});  

  	it('should have a total score field', function () {  	
    	const wrapper = shallow(<ScoreCard {...props} />);  
    	expect(wrapper.find('TextField')).to.have.length( 1 );
  	});  

  	it('should have initial 10 of list question&answers', function () {
		const wrapper = shallow(<ScoreCard {...props} />);  
	    expect(wrapper.state().itemGroups).to.have.length(10);
	});

	it('should render Markdown in preview mode', () => {

		const item_props = {                                                                                                                                                   
		    group: {
		    	title:"Feeding", 
		    	selectedValue:"", 
		    	items:[
		    		{name:"Unable", weight:10},
		    		{name:"Independent", weight:50}
		    	]
		    },
		    groupIndex:0,
			selectItemWeight:()=>{}
		};
		const wrapper = shallow(<ScoreCard {...props} />);  
		const wrapper_item = shallow(<ScoreCardItem  {...item_props } />);
		wrapper_item.find('RadioButtonGroup').simulate('change',null,'Unable');
		expect(wrapper.state().itemGroups).to.have.length(10);
	});
  	
});