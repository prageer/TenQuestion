export const SET_DATE = 'SET_DATE';
export const SET_PRACTITIONER_ID = 'SET_PRACTITIONER_ID';
export const SET_PATIENT_ID = 'SET_PATIENT_ID';
export const SET_ANSWER = 'SET_ANSWER';